public with sharing class CaseListController {
	public List<Case> Cases{get;set;}

	public CaseListController(){
		Cases = [select Id, CaseNumber, Subject, Status from Case where isClosed = false];
	}


}